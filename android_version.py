#
# Copyright (C) 2017 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import re
import sys
import utils

patch_level = '4'
_remote_name = ''
_branch_name = ''
_branch_name_modifier = ''
_svn_revision = ''


def _get_llvm_remote_and_branch_info():
    global _remote_name
    global _branch_name
    global _branch_name_modifier

    if _branch_name == '':
        llvm_repo_dir = utils.android_path('toolchain', 'llvm-project')

        remote_refs = utils.check_output(
                ['git', 'for-each-ref', 'refs/remotes/', '--format', '%(refname)'],
                cwd=llvm_repo_dir).splitlines()

        number_of_commits_added_candidate = -1
        ref_candidate = ''

        for remote_ref in remote_refs:
            if remote_ref.startswith('refs/remotes/m/'):
                continue

            remote_ref_commit_hash = utils.check_output(
                    ['git', 'rev-parse', remote_ref],
                    cwd=llvm_repo_dir).strip()

            merge_base_commit_hash = utils.check_output(
                    ['git', 'merge-base', remote_ref, 'HEAD'],
                    cwd=llvm_repo_dir).strip()

            if merge_base_commit_hash == remote_ref_commit_hash:
                number_of_commits_added_str = utils.check_output(
                        ['git', 'rev-list', '--count', '%s..%s' % (merge_base_commit_hash, 'HEAD')],
                        cwd=llvm_repo_dir).strip()
                number_of_commits_added = int(number_of_commits_added_str)

                if ref_candidate == '' or number_of_commits_added_candidate > number_of_commits_added:
                    ref_candidate = remote_ref
                    number_of_commits_added_candidate = number_of_commits_added

        if ref_candidate == '':
            raise Exception('No matching remote ref found.')

        branch_name_suffixes = []
        if number_of_commits_added_candidate != 0:
            head_short_commit_hash = utils.check_output(
                    ['git', 'rev-parse', '--short', 'HEAD'],
                    cwd=llvm_repo_dir).strip()

            branch_name_suffixes += ['%s-g%s' % (number_of_commits_added_candidate, head_short_commit_hash)]

        is_clean = not utils.check_output(['git', 'status', '-z'], cwd=llvm_repo_dir)

        if not is_clean:
            branch_name_suffixes += ['modified']

        match = re.match('refs\/remotes\/([^/]+)\/(.+)', ref_candidate)

        if match == None:
            raise ValueError("Can't parse branch name: '" + full_branch_name + "'")

        _remote_name = match.group(1)
        _branch_name = match.group(2)
        _branch_name_modifier = '-'.join(branch_name_suffixes)

    return _remote_name, _branch_name, _branch_name_modifier

def _get_svn_revision(build_llvm_next=False):
    if build_llvm_next:
        raise ValueError('build_llvm_next mode unsupported')

    global _svn_revision

    if _svn_revision == '':
        llvm_repo_dir = utils.android_path('toolchain', 'llvm-project')

        remote_name, branch_name, branch_name_modifier = _get_llvm_remote_and_branch_info()

        git_llvm_rev_cmd = [
            sys.executable,
            utils.android_path('external', 'toolchain-utils', 'llvm_tools', 'git_llvm_rev.py'),
            '--llvm_dir', llvm_repo_dir,
            '--upstream', remote_name,
            '--sha', 'HEAD'
        ]

        _svn_revision = utils.check_output(git_llvm_rev_cmd).strip()

    return _svn_revision

def get_svn_branch_revision_tuple(llvm_next: bool=False):
    remote_name, branch_name, branch_name_modifier = _get_llvm_remote_and_branch_info()
    svn_revision = _get_svn_revision(llvm_next)

    return branch_name, branch_name_modifier, svn_revision

def get_svn_branch_revision_str(llvm_next: bool=False):
    branch_name, branch_name_modifier, svn_revision = get_svn_branch_revision_tuple(llvm_next)

    revision_str = branch_name

    if branch_name_modifier:
        revision_str += '-' + branch_name_modifier

    revision_str += '-' + svn_revision

    return revision_str
